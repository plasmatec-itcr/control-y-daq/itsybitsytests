import control_host as ch
import asyncio
from tkinter import *
from matplotlib.figure import Figure
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from threading import Thread

from datetime import datetime
from time import perf_counter_ns
from time import sleep


def parse_scinumber(num):
     sign = "+" if num.epositive else "-"
     return "{}.{:02d}E{}{}".format(num.integer, num.decimal, sign, num.exponent)

class grafico:
    def __init__(self, root,poll_val,coords, x = [], y= [], size = (6,4), ylim = (10,100), wait_time=100, semilogy = False):
        self.x = x
        self.y = y
        self.resetx = x
        self.resety = y
        self.i = 0
        self.fig = Figure(figsize=size, dpi = 100)
        self.ax = self.fig.add_subplot()
        if semilogy:
            self.line, = self.ax.semilogy(x,y)
        else :
            self.line, = self.ax.plot(x,y)
        self.ax.set_ylim(ylim)

        # canvas de tkinter
        self.canvas = FigureCanvasTkAgg(self.fig, master = root)
        self.canvas.draw()
        self.animation = FuncAnimation(self.fig, self.update, repeat = False,blit=False, interval=wait_time)
        self.poll_val = poll_val
        self.root = root
        self.canvas.get_tk_widget().place(x=coords[0], y=coords[1])
        self.activo = True
    
    def update(self,frame):
        self.x.append(self.i)
        self.i = self.i+1
        self.y.append(self.poll_val())
        self.x = self.x[-200:]
        self.y = self.y[-200:]
        self.line.set_data(self.x, self.y)
        self.ax.relim()
        self.ax.autoscale_view(True)
        return self.line,

    def toggle(self):
        if self.activo:
            self.animation.pause()
            self.x = self.resetx
            self.y = self.resety
            self.activo = False
        else:
            self.activo = True
            self.animation.resume()



# ventana de tkinter
root = Tk()
root.minsize(1800,900)
root.resizable(width=NO, height=NO)
pantalla = Canvas(root, width=1800, height=900, bg='white')
pantalla.place(x=0,y=0)

font = ("Envy Code R", 19)

temp_label = pantalla.create_text(300, 500, font = ("Envy Code R", 25))
presion_label = pantalla.create_text(300, 550, font = ("Envy Code R", 25))
flujo_label = pantalla.create_text(300, 600, font = ("Envy Code R", 25))
inyeccion_label = pantalla.create_text(1500, 530, font = ("Envy Code R", 21))

pantalla.itemconfigure(flujo_label, text = "Flujo _.__ SCCM")
pantalla.itemconfigure(presion_label, text = "Presión: _.__E+__  Torr")
pantalla.itemconfigure(temp_label, text = "Temperatura: _ °C")
pantalla.itemconfigure(inyeccion_label, text = "NO SE ESTÁ INYECTANDO GAS")
##### datos
schema = ch.image('../target/thumbv7em-none-eabihf/release/control-device-led')
port = ch.open_serial(schema, "/dev/ttyACM0", 192000)


temperatura = 0.0
presion = 0.0
flujo = 0.0
RUNNING = True
LOGGING = True


async def wait_timeout(task):
    return await asyncio.shield(asyncio.wait_for(task, timeout = 1.0))

async def t_temperatura():
    global port, temperatura,pantalla, temp_label, RUNNING
    try:
        await asyncio.sleep(0)
        var = await wait_timeout(port.temp.get())
        temperatura = var#/10.0
        pantalla.itemconfigure(temp_label, text = "Temperatura: {} °C".format(temperatura))
    except: 
        print("temperatura desconectada")
        await asyncio.sleep(4)

async def t_presion():
    global port, presion, RUNNING 
    try:
        await asyncio.sleep(0)
        presion_raw = parse_scinumber(await wait_timeout(port.mks979b.pressure()))
        presion = float(presion_raw)
        pantalla.itemconfigure(presion_label, text = "Presión: {} Torr".format(presion_raw))
    except Exception as e:
        print(e)
        print("presion desconectada")
        await asyncio.sleep(4)

async def t_flujo():
    global port, flujo, RUNNING
    try:
        await asyncio.sleep(0)
        x = await wait_timeout(port.pr4000b.flow_value1())
        #valor y punto decimal lo extraigo
        flujo = x.value / (10**x.decimal_point)
        pantalla.itemconfigure(flujo_label, text = "Flujo {} SCCM".format(flujo))
    except: 
        print("flujo desconectado")
        await asyncio.sleep(4)

MISC_TASKS = []

async def misc():
    global MISC_TASKS
    for i in MISC_TASKS:
        print("awaiting a task")
        await MISC_TASKS.pop()()
        print("task completed")
        
async def encender_inyeccion():
    global port,btn_log_on, btn_log_off
    try:
        exito = await wait_timeout(port.pr4000b.injection(turn_on= True))
        if exito:
            btn_inyeccion_off["state"] = "normal"
            btn_inyeccion_on["state"] = "disabled"
            pantalla.itemconfigure(inyeccion_label, text = "SE ESTÁ INYECTANDO GAS")
        else:
            pantalla.itemconfigure(inyeccion_label, text = "Algo falló al iniciar inyección de gas")
    except Exception as e:
        print(e)
        print("No hay comunicación con el sistema de inyección de flujo")

async def apagar_inyeccion():
    global port,btn_log_on, btn_log_off
    try:
        exito = await wait_timeout(port.pr4000b.injection(turn_on= False))
        if exito:
            btn_inyeccion_on["state"] = "normal"
            btn_inyeccion_off["state"] = "disabled"
            pantalla.itemconfigure(inyeccion_label, text = "NO SE ESTÁ INYECTANDO GAS")
        else:
            pantalla.itemconfigure(inyeccion_label, text = "Algo falló al iniciar inyección de gas")
            
    except: 
        print("No hay comunicación con el sistema de inyección de flujo")


def log_data():
    global LOGGING
    LOGGING = True
    filename = "./datos_{}.csv".format(datetime.now())
    f = open(filename, "w")
    f.write("Tiempo en ns, Temperatura °C,Presion Torr,Flujo SCCM\n")
    while LOGGING:
        f.write("{},{},{},{}\n".format(
            perf_counter_ns(),
            temperatura, 
            presion,
            flujo)
        )
        sleep(0.5)
    print("finalizar log de datos")
    f.close()




# crear gráfico
x,y  = [-x for x in range(200, 1, -1)], [25]*199
temp_plot = grafico(pantalla, lambda: temperatura, (0,0), x,y, ylim = (0, 50))
pressure_plot = grafico(pantalla, lambda: presion, (600,0), x,y, semilogy = True, ylim = (1e-5, 10e+2))
flow_plot = grafico(pantalla, lambda: flujo, (1200,0), x,y,ylim = (0, 30))

async def mediciones():
    global RUNNING
    while RUNNING:
        await asyncio.gather(
            t_temperatura(),
            t_presion(),
            #t_flujo(),
            misc()
        )
    print("fin programa")


def t_mediciones(): 
    asyncio.run(mediciones())

Thread(target = t_mediciones, args=()).start()


def hilo_encender_inyeccion():
    global MISC_TASKS
    MISC_TASKS.append(encender_inyeccion)

def hilo_apagar_inyeccion():
    global MISC_TASKS
    MISC_TASKS.append(apagar_inyeccion)

def close():
    global RUNNING, LOGGING
    RUNNING = False
    LOGGING = False
    root.after(10, root.destroy)

root.protocol("WM_DELETE_WINDOW", close)



Button(root,width=40, text = "toggle gráfico", command = lambda: temp_plot.toggle(), font = font).place(x = 40, y = 410)
Button(root,width=40, text = "toggle gráfico ", command = lambda: pressure_plot.toggle(), font = font).place(x = 640, y = 410)
Button(root,width=40, text = "toggle gráfico", command = lambda: flow_plot.toggle(), font = font).place(x = 1220, y = 410)
btn_inyeccion_on =  Button(root,width=20, text = "inyectar gases", command = hilo_encender_inyeccion,font = font)
btn_inyeccion_on.place(x = 1350, y = 570)
btn_inyeccion_off = Button(root,width=40, text = "detener inyección de gases", state = "disabled",command = hilo_apagar_inyeccion,font = font)
btn_inyeccion_off.place(x = 1230, y =630)

def finalizar_reg_datos():
    global LOGGING, btn_log_on, btn_log_off
    btn_log_on["state"] = "normal"
    btn_log_off["state"] = "disabled"
    LOGGING = False

def empezar_reg_datos(): 
    global btn_log_on, btn_log_off
    btn_log_on["state"] = "disabled"
    btn_log_off["state"] = "normal"
    Thread(target = log_data, args = ()).start()

btn_log_on = Button(root,height=3, text = "Empezar registro de datos", command = empezar_reg_datos, font = font)
btn_log_on.place(x = 470, y = 750)
btn_log_off = Button(root,height=3, text = "Finalizar registro de datos", state ="disabled" ,command = finalizar_reg_datos, font = font)
btn_log_off.place(x = 850, y = 750)

root.bind('<Button-1>', lambda e: print("coords: x= {}, y= {}".format(e.x, e.y)))

root.mainloop()


