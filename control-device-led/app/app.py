import control_host as ch
import asyncio as aio


def parse_scinumber(num):
	integer = num >> 24
	decimal = (num & 0xff0000) >> 16 
	epositivo = (num & 0xff00) >> 8 
	if epositivo == 1:
		epositivo = "+"
	else:
		epositivo = "-"
	exponente = num & 0xff
	return float("{}.{}E{}{}".format(integer, decimal, epositivo, exponente))

schema = ch.image('../target/thumbv7em-none-eabihf/release/control-device-led')
port = ch.open_serial(schema, "/dev/ttyACM0", 192000)


async def flow():
    a =  await port.pr4000b.flow_value1()
    return a
async def qremote():
    a =  await port.pr4000b.query_remote()
    return a
async def remote1():
    a =  await port.pr4000b.remote(turn_on=False)
    return a
async def remote2():
    a =  await port.pr4000b.injection(turn_on=False)
    return a
async def ka():
    a =  await port.pr4000b.injection(turn_on=True)
    return a
async def kr():
    a =  await port.mks979b.pressure()
    return a
queryremote = lambda: aio.run(qremote())
remoteno = lambda: aio.run(remote1())
remoteyes= lambda: aio.run(remote2())
c = lambda: aio.run(kc())
b3 = lambda: aio.run(kr())

