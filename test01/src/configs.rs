use crate::*;
pub struct USBSerial<'a> {
    serial: SerialPort<'a, UsbBus>,
    bus: UsbDevice<'a, UsbBus>,
}
impl<'a> USBSerial<'a> {
    pub fn new(bus_allocator: &'a UsbBusAllocator<UsbBus>) -> Self {
        USBSerial {
            serial: SerialPort::new(bus_allocator),
            bus: UsbDeviceBuilder::new(bus_allocator, UsbVidPid(0x16c0, 0x27dd))
                .manufacturer("Fake company")
                .product("Serial port")
                .serial_number("TEST")
                .device_class(USB_CLASS_CDC)
                .build(),
        }
    }
    pub fn poll(&mut self) {
        self.bus.poll(&mut [&mut self.serial]);
    }
    pub fn flush(&mut self) {
        let _ = self.serial.flush();
    }
    pub fn read(&mut self, buf: &mut [u8]) -> Result<usize, UsbError> {
        self.serial.read(buf)
    }
}

impl Write for USBSerial<'_> {
    fn write_str(&mut self, string: &str) -> core::fmt::Result {
        let mut string = string.as_bytes();

        while !string.is_empty() {
            match self.serial.write(string) {
                Ok(count) => string = &string[count..],
                Err(UsbError::WouldBlock) => continue,
                Err(_) => return Err(fmt::Error),
            }
        }

        Ok(())
    }
}
pub struct Termocupla {
    adc: InterruptAdc<ADC1, FreeRunning>,
    pin: Pin<PB08, AlternateB>,
}

impl Termocupla {
    pub fn new(
        adc: impl Into<InterruptAdc<ADC1, FreeRunning>>,
        pin: impl Into<Pin<PB08, AlternateB>>,
    ) -> Self {
        Termocupla {
            adc: adc.into(),
            pin: pin.into(),
        }
    }
    pub fn start_sampling(&mut self) {
        self.adc.start_conversion(&mut self.pin);
    }
    pub fn temp(&mut self) -> Option<u16> {
        self.adc.service_interrupt_ready()
    }
}

pub type UartPads0 = uart::Pads<Sercom0, IoSet3, IoSet3Sercom0Pad2, IoSet3Sercom0Pad0>;
pub type Uart0 = uart::Uart<uart::Config<UartPads0>, uart::Duplex>;

/// Utility function for setting up SERCOM0 pins as an additional
/// UART peripheral.
pub fn uart0(
    clocks: &mut GenericClockController,
    baud: impl Into<Hertz>,
    sercom0: pac::SERCOM0,
    mclk: &mut pac::MCLK,
    uart_rx: impl Into<IoSet3Sercom0Pad2>,
    uart_tx: impl Into<IoSet3Sercom0Pad0>,
) -> Uart0 {
    let gclk0 = clocks.gclk0();
    let clock = &clocks.sercom0_core(&gclk0).unwrap();
    let baud = baud.into();
    let pads = uart::Pads::default().rx(uart_rx.into()).tx(uart_tx.into());
    //Configure UART for use with the PR4000B with Odd parity
    uart::Config::new(mclk, sercom0, pads, clock.freq())
        .parity(Parity::Odd)
        .baud(baud, BaudMode::Fractional(Oversampling::Bits16))
        .enable()
}
