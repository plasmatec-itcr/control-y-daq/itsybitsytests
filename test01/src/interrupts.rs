use crate::*;
fn poll_usb() {
    unsafe {
        if let Some(usb) = USB_SERIAL.as_mut() {
            usb.poll();
            usb.flush();

            let mut buf = [0u8; 64];
            if let Ok(count) = usb.read(&mut buf) {
                for (i, c) in buf.iter().enumerate() {
                    if i >= count {
                        break;
                    }
                    let _ = write!(usb, "{}", c);
                }
            }
        }
    }
}

#[interrupt]
fn USB_OTHER() {
    poll_usb();
}

#[interrupt]
fn USB_TRCPT0() {
    poll_usb();
}

#[interrupt]
fn USB_TRCPT1() {
    poll_usb();
}

#[interrupt]
fn SERCOM3_2() {
    unsafe {
        if let Some(x) = MKS979B.as_mut() {
            x.poll();
        }
    }
}

fn toggle() {
    unsafe {
        if let Some(x) = LED.as_mut() {
            x.toggle().unwrap();
        }
    }
}

fn check_temp() {
    unsafe {
        if let Some(termocupla) = TERMOCUPLA.as_mut() {
            if let Some(adc_val) = termocupla.temp() {
                //let adc_val: u16 = termocupla.adc.read(&mut termocupla.pin_termocupla).unwrap();
                let nano_volts = adc_val as u32 * 805861; // = 3.3e6 / 4095;
                TEMPERATURA_C = (nano_volts - 1_250_000_000) / 5_000_000;
            }
        } else {
            toggle();
        }
    }
}

#[interrupt]
fn ADC1_RESRDY() {
    toggle();
    check_temp();
}

#[exception]
fn SysTick() {
    unsafe {
        if let Some(usb) = USB_SERIAL.as_mut() {
            let _ = write!(usb, "{{'data' : [");

            if let Some(mks979b) = MKS979B.as_mut() {
                if let Ok(Some(json_resp)) = mks979b.poll_json_response(false) {
                    let _ = write!(usb, "{{{}}},", json_resp);
                }
            }

            if let Some(poll) = PR4000B_POLL.as_mut() {
                if let Ok(ok) = poll.poll() {
                    let _ = write!(usb, "pr4000b: {:?}", ok);
                }
            }

            let _ = write!(usb, "{{'temp':{}}}]}}", TEMPERATURA_C);
        }
    }
}
