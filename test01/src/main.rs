#![no_std]
#![no_main]

mod configs;
mod interrupts;

use bsp::{
    hal,
    uart::{self, Parity},
    BaudMode, IoSet3, IoSet3Sercom0Pad0, IoSet3Sercom0Pad2, Oversampling,
};
use itsybitsy_m4 as bsp;
#[cfg(not(feature = "use_semihosting"))]
use panic_halt as _;
#[cfg(feature = "use_semihosting")]
use panic_semihosting as _;

use bsp::entry;
use core::fmt::{self, Write};
use cortex_m::peripheral::NVIC;
use cortex_m_rt::exception;
use hal::{
    clock::GenericClockController,
    ehal::digital::v2::ToggleableOutputPin,
    gpio::v2::*,
    pac::{self, gclk::pchctrl::GEN_A, interrupt, CorePeripherals, Peripherals, ADC1},
    sercom::v2::Sercom0,
    thumbv7em::adc::{Adc, FreeRunning, InterruptAdc},
    time::Hertz,
    usb::{
        usb_device::{bus::UsbBusAllocator, prelude::*},
        UsbBus,
    },
};
use usbd_serial::{SerialPort, USB_CLASS_CDC};

use mks979b::{
    cmd::{Message, Setting, Value},
    fsm::{self, Mks979b},
};
use pr4000b::{data, param, Pr4000b};

use configs::*;

static mut MKS979B: Option<Mks979b<bsp::Uart>> = None;
static mut PR4000B: Option<Pr4000b<Uart0>> = None;
static mut PR4000B_POLL: Option<
    pr4000b::cmd::Poll<
        bsp::uart::Uart<
            itsybitsy_m4::uart::Config<
                itsybitsy_m4::uart::Pads<
                    pac::SERCOM0,
                    itsybitsy_m4::IoSet3,
                    itsybitsy_m4::hal::gpio::v2::Pin<
                        PA06,
                        itsybitsy_m4::hal::gpio::v2::Alternate<itsybitsy_m4::hal::gpio::v2::D>,
                    >,
                    itsybitsy_m4::hal::gpio::v2::Pin<
                        PA04,
                        itsybitsy_m4::hal::gpio::v2::Alternate<itsybitsy_m4::hal::gpio::v2::D>,
                    >,
                >,
            >,
            itsybitsy_m4::uart::Duplex,
        >,
        data::Setpoint,
    >,
> = None;
static mut LED: Option<hal::gpio::v2::Pin<PA22, hal::gpio::v2::PushPullOutput>> = None;
static mut USB_SERIAL: Option<USBSerial> = None;
static mut USB_BUS_ALLOCATOR: Option<UsbBusAllocator<UsbBus>> = None;
static mut MKS979_COMMANDS: Option<heapless::Vec<Message, 4>> = None;
static mut TERMOCUPLA: Option<Termocupla> = None;
static mut TEMPERATURA_C: u32 = 25;

#[entry]
fn main() -> ! {
    //Inicialización del sistema
    let mut dp = Peripherals::take().unwrap();
    let mut core = CorePeripherals::take().unwrap();
    let mut clocks = GenericClockController::with_internal_32kosc(
        dp.GCLK,
        &mut dp.MCLK,
        &mut dp.OSC32KCTRL,
        &mut dp.OSCCTRL,
        &mut dp.NVMCTRL,
    );
    let pins = bsp::Pins::new(dp.PORT);

    //Led que se mantiene parpadeando cuando el sistema funciona
    unsafe {
        LED = Some(pins.d13.into_push_pull_output());
    }

    //Configuración de termocupla
    let adc = Adc::adc1(dp.ADC1, &mut dp.MCLK, &mut clocks, GEN_A::GCLK11);
    let mut termopar = Termocupla::new(adc, pins.a2);
    termopar.start_sampling();
    unsafe {
        TERMOCUPLA = Some(termopar);
    }

    //Configuración de puerto para sacar información
    let bus_allocator = unsafe {
        USB_BUS_ALLOCATOR = Some(bsp::usb_allocator(
            dp.USB,
            &mut clocks,
            &mut dp.MCLK,
            pins.usb_dm,
            pins.usb_dp,
        ));
        USB_BUS_ALLOCATOR.as_ref().unwrap()
    };

    unsafe {
        USB_SERIAL = Some(USBSerial::new(bus_allocator));
    }

    //Configuración para el MKS979B
    let mut serial3 = bsp::uart(
        &mut clocks,
        Hertz(9600),
        dp.SERCOM3,
        &mut dp.MCLK,
        pins.d0_rx,
        pins.d1_tx,
    );
    serial3.enable_interrupts(hal::sercom::v2::uart::Flags::RXC);
    let mut transducer = fsm::Mks979b::try_new(serial3, 253).unwrap();
    let mut list: heapless::Vec<Message, 4> = heapless::Vec::new();
    list.push(Setting::TestLedON(true).into()).unwrap();
    list.push(Value::TestLedON.into()).unwrap();
    list.push(Setting::TestLedON(false).into()).unwrap();
    list.push(Value::TestLedON.into()).unwrap();
    for kk in list {
        transducer.send_message(kk).unwrap();
    }
    unsafe {
        //MKS979_COMMANDS = Some(list);
        MKS979B = Some(transducer)
    };

    //Configuración para el PR4000B
    let mut serial0 = uart0(
        &mut clocks,
        Hertz(9600),
        dp.SERCOM0,
        &mut dp.MCLK,
        pins.a5,
        pins.a4,
    );
    serial0.enable_interrupts(hal::sercom::v2::uart::Flags::RXC);
    unsafe {
        PR4000B = Some(Pr4000b::new(serial0));
        PR4000B_POLL = Some(
            PR4000B
                .as_mut()
                .unwrap()
                .param(param::Setpoint)
                .channel(param::Channel1)
                .read(),
        );
    };

    cortex_m::interrupt::free(|_| {
        unsafe {
            core.NVIC.set_priority(interrupt::USB_OTHER, 1);
            core.NVIC.set_priority(interrupt::USB_TRCPT0, 1);
            core.NVIC.set_priority(interrupt::USB_TRCPT1, 1);
            core.NVIC.set_priority(interrupt::ADC1_RESRDY, 1);
            core.NVIC.set_priority(interrupt::SERCOM3_2, 1);

            NVIC::unmask(interrupt::USB_OTHER);
            NVIC::unmask(interrupt::USB_TRCPT0);
            NVIC::unmask(interrupt::USB_TRCPT1);
            NVIC::unmask(interrupt::SERCOM3_2);
            NVIC::unmask(interrupt::ADC1_RESRDY);
        }

        let mut syst = core.SYST;
        syst.set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
        syst.set_reload(12_000_000); //1ms
        syst.enable_counter();
        syst.enable_interrupt();
    });
    loop {}
}
